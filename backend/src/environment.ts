import fs from 'fs';
import dotenv from 'dotenv';

dotenv.config();

const secretFileString = '/run/secrets';

const secrets = {
  INSTAGRAM_USER_ID: process.env.INSTAGRAM_USER_ID?.startsWith(secretFileString) ? fs.readFileSync(process.env.INSTAGRAM_USER_ID, 'utf8') : process.env.INSTAGRAM_USER_ID,
  INSTAGRAM_ACCESS_TOKEN: process.env.INSTAGRAM_ACCESS_TOKEN?.startsWith(secretFileString) ? fs.readFileSync(process.env.INSTAGRAM_ACCESS_TOKEN, 'utf8') : process.env.INSTAGRAM_ACCESS_TOKEN,
};

const environment: any = {
  ...process.env,
  ...secrets,
};

export default environment;
