export type EventResponse = {
  count: number;
  next: any;
  previous: any;
  results: EventAttributes[];
}

type EventTag = {
  id: number;
  slug: string;
  name: string;
  name_fi: string;
  name_en: string;
  icon: string;
}

type EventSignupForm = {
  id: number;
  title: string;
  start_time: string;
  end_time: string;
  questions: string;
}

export type EventAttributes = {
  id: number;
  tag_id: number[];
  tags: EventTag[];
  visible: boolean;
  title_fi: string;
  title_en: string;
  description_fi: string;
  description_en: string;
  content_fi: string;
  content_en: string;
  start_time: string;
  end_time: string;
  location: string;
  signup_id: number[];
  signupForm: EventSignupForm[];
}
