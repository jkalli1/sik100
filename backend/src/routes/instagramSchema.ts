type InstagramPost = {
  caption: string;
  media_type: string;
  media_url: string;
  permalink: string;
  timestamp: string;
  id: string;
}

export type InstagramResponse = {
  data: InstagramPost[];
}
