import express from 'express';
import axios, { AxiosResponse } from 'axios';
import environment from '../environment';
import { EventResponse, EventAttributes } from './eventSchema';
import { InstagramResponse } from './instagramSchema';

const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => res.status(200).json({ msg: 'SIK100 API' }));

const splitContentIntoParagraphs = (event: EventAttributes) => ({
  ...event,
  content_fi: event.content_fi.split(/\r?\n/).filter(x => x !== ''),
  content_en: event.content_en.split(/\r?\n/).filter(x => x !== ''),
});

router.get('/events', async (req, res, next) => {
  const url = `${environment.EVENT_BACKEND}/events/?tags=${environment.EVENT_TAG_ID}`;
  try {
    const resp: AxiosResponse<EventResponse> = await axios.get(url);
    const { data: { results } } = resp;
    return res.status(200).json(results.map(splitContentIntoParagraphs));
  } catch (err) {
    const error = 'Failed to get all events';
    // eslint-disable-next-line
    console.error(error);
    // eslint-disable-next-line
    console.error(err);
    return res.status(500).json({ error });
  }
});

router.get('/instagram', async (req, res, next) => {
  const { count } = req.query;
  if (!count) return res.status(400).json({ error: 'Missing "count" query paramter' });
  const userId = environment.INSTAGRAM_USER_ID;
  const accessToken = environment.INSTAGRAM_ACCESS_TOKEN;
  const fields = [
    'children',
    'caption',
    'media_type',
    'media_url',
    'permalink',
    'timestamp',
    // 'like_count',
  ].join(',');
  const url = `https://graph.instagram.com/${userId}/media?fields=${fields}&access_token=${accessToken}&limit=${count}`;

  // Refresh API token, it lives 60 days
  axios.get(`https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=${accessToken}`).catch((err) => {
    // eslint-disable-next-line
    console.error(err);
  });

  // Actual query
  try {
    const resp: AxiosResponse<InstagramResponse> = await axios.get(url);
    const { data } = resp.data;
    return res.status(200).json(
      {
        data: data.map(obj => ({
          img: obj.media_url,
          link: obj.permalink,
          // tags: obj.tags,
          text: obj.caption,
          id: obj.id,
          // likes: obj.likes.count,
        })),
      },
    );
  } catch (err) {
    const error = 'Failed to get Instagram feed data.';
    // eslint-disable-next-line
    console.error(error);
    // eslint-disable-next-line
    console.error(err);
    return res.status(500).json({ error });
  }
});

export default router;
