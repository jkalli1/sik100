import React from "react";
import LazyLoad from "react-lazyload";
import styled from "../styled";
import useFetchInstagram from "../hooks/useFetchInstagram";

const Title = styled.h2`
  color: ${(p) => p.theme.colors.yellow};
  text-transform: uppercase;
  text-align: center;
`;

const Instagram = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
`;

const InstagramContainer = styled.div`
  color: ${(p) => p.theme.colors.white};
  margin: ${(p) => p.theme.spacing.sm};
  width: min-content;
  display: inline-table;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
    margin: 0;
  }
`;

const ImageContainer = styled.div`
  height: 250px;
  width: 250px;
  background-color: black;
  display: flex;
  align-items: center;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxxs}) {
    height: 200px;
    width: 200px;
  }
`;

const Image = styled.img`
  width: 250px;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxxs}) {
    width: 200px;
  }
`;

const ImgCaption = styled.p`
  color: ${(p) => p.theme.colors.white};
  font-size: 14px;
  text-decoration: none;
`;

const InstagramFeed: React.FC = () => {
  const images = useFetchInstagram();
  return (
    <>
      <Title>Seuraa sosiaalisessa mediassa</Title>
      <Instagram>
        {images.map((image) => (
          <InstagramContainer key={image.id}>
            <a href={image.link}>
              <ImageContainer>
                <LazyLoad height={250}>
                  <Image src={image.img} />
                </LazyLoad>
              </ImageContainer>
            </a>
            <ImgCaption>{image.text}</ImgCaption>
          </InstagramContainer>
        ))}
      </Instagram>
    </>
  );
};

export default InstagramFeed;
