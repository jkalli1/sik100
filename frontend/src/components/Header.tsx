import React from "react";
import styled from "../styled";

const Container = styled.h1`
  background-color: ${(p) => p.theme.colors.turquoise};
`;

const Header: React.FC = ({ children }) => (
  <Container>
    {children}
  </Container>
);

export default Header;
