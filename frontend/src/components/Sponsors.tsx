import React from "react";
import styled from "../styled";

const BG = styled.div`
  background: ${(p) => p.theme.colors.blue4};
  padding: ${(p) => p.theme.spacing.md};
`;

const Container = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  align-items: center;
  ${(p) => (
    `margin: ${p.theme.spacing.md};
    @media screen and (max-width: ${p.theme.breakpoints.xxs}) {
      margin: ${p.theme.spacing.xs}
    };`)}
`;

const Sponsor = styled.div`
  background: red;
  height: 200px;
  width: 200px;
  margin: ${(p) => p.theme.spacing.xs};
`;
const SponsorTitle = styled.h2`
  color: ${(p) => p.theme.colors.yellow};
  text-transform: uppercase;
  text-align: center;
  padding: ${(p) => p.theme.spacing.sm};
`;

const Sponsors: React.FC = () => (
  <BG>
    <SponsorTitle>Juhlavuotta tukemassa</SponsorTitle>
    <Container>
      <Sponsor />
      <Sponsor />
      <Sponsor />
      <Sponsor />
    </Container>
  </BG>
);

export default Sponsors;
