import React from "react";
import styled from "../styled";

const Container = styled.div`
  background-color: ${(p) => p.theme.colors.darkBlue2};
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Content = styled.div`
  ${(p) => p.theme.margins.page};
  max-width: ${(p) => p.theme.contentWidth};
  width: 100%;
`;

const PageContent: React.FC = ({ children }) => (
  <Container>
    <Content>
      {children}
    </Content>
  </Container>
);

export default PageContent;
