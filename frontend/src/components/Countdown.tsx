import React, { useReducer, useEffect } from "react";
import styled from "../styled";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  font: 'Montserrat', sans-serif;
  background-color: ${(p) => p.theme.colors.darkBlue1};
  color: ${(p) => p.theme.colors.white};
  height: 100%;
`;

const P = styled.p`
  font-size: 48px;

  @media screen and (max-width: 820px) {
    font-size: 32px;
  }
`;

const Time = styled.p`
  font-weight: 900;
  font-size: 50px;
  color: ${(p) => p.theme.colors.turquoise};

  @media screen and (max-width: 820px) {
    font-size: 40px;
  }
`;

const StartLinkText = styled.p`
  color: ${(p) => p.theme.colors.white};
  font-size: 32px;
`;

const StartLink = styled.a`
  color: ${(p) => p.theme.colors.primaryGold};
`;

const countdownReducer = (state: CountdownState): CountdownState => (
  {
    ...state,
    timeLeft: state.timeLeft - 1,
  }
);

interface CountdownProps {
  revealDate: Date;
}

interface CountdownState {
  timeLeft: number;
}

const Countdown: React.FC<CountdownProps> = ({ revealDate, children }): JSX.Element => {
  const [state, dispatch] = useReducer(countdownReducer, { timeLeft: Math.floor((revealDate.valueOf() - new Date().valueOf()) / 1000) });

  useEffect(() => {
    const intervalHandle = setInterval(dispatch, 1000);
    return (): void => { clearInterval(intervalHandle); };
  }, []);

  if (state.timeLeft > 0) {
    const days = Math.floor(state.timeLeft / 86400);
    const hours = Math.floor((state.timeLeft - days * 86400) / 3600);
    const minutes = Math.floor((state.timeLeft - days * 86400 - hours * 3600) / 60);
    const seconds = (state.timeLeft - days * 86400 - hours * 3600 - minutes * 60);
    return (
      <Container>
        <P>SIK100-vuoden alkuun</P>
        <Time>{`${days}:${hours}:${minutes}:${seconds}`}</Time>
        <StartLinkText>
          Ilmoittaudu juhlavuoden aloitustapahtumaan&nbsp;
          <StartLink href="https://sik.ayy.fi/fi/events/4523/SIK100+Alkupamaus">täältä!</StartLink>
        </StartLinkText>
      </Container>
    );
  }
  return (
    <>{children}</>
  );
};

export default Countdown;
