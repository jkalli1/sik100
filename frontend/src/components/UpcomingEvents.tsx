import React from "react";
import { Link } from "react-router-dom";
import styled from "../styled";
import Event from "../state/domain/events";
import { EventCard } from "./EventCard";

const Container = styled.div`
  margin: ${(p) => p.theme.spacing.sm};
`;

const CardContainer = styled.div`
  margin: ${(p) => p.theme.spacing.lg} 0;
`;

const EventsLink = styled.span`
  text-transform: uppercase;
  text-decoration: none;
  font-weight: bold;
  color: ${(p) => p.theme.colors.white};
`;

const EmptyEvent = styled.div`
  display: flex;
  min-height: 103px;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xs}) {
    min-height: 90px;
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
    min-height: 80px;
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxxs}) {
    min-height: 72px;
  }

  width: 100%;
  margin: ${(p) => p.theme.spacing.sm} 0;
  background: linear-gradient(to right, #000, #fff, #000);
  background-size: 200% 100%;
  animation: AnimationName 3s linear infinite;

  @keyframes AnimationName {
    0% { background-position: 0% 0%; }
    50% { background-position: 100% 0%; }
    100% { background-position: 200% 0%; }
  }
`;

const Arrow = styled.div`
  display: inline-block;
  margin-left: 12px;
  width: 0;
  height: 0;
  border-top: 6px solid transparent;
  border-bottom: 6px solid transparent;
  border-left: 6px solid ${(p) => p.theme.colors.primaryGold};
`;

type UpcomingEventsProps = {
  events: Event[];
  link?: boolean;
}

const UpcomingEvents: React.FC<UpcomingEventsProps> = ({
  events, link, children,
}) => {
  let eventRender;
  if (events.length > 0) {
    eventRender = events.map((e) => (
      <EventCard
        key={e.id}
        title={e.title_fi}
        start={new Date(e.start_time).toLocaleDateString("fi-FI", { day: "numeric", month: "numeric" })}
        location={e.location}
        content={e.content_fi}
      />
    ));
  } else {
    eventRender = [
      <EmptyEvent key="empty1" />,
      <EmptyEvent key="empty2" />,
      <EmptyEvent key="empty3" />,
    ];
  }
  return (
    <Container>
      {children}
      <CardContainer>{eventRender}</CardContainer>

      { link && (
        <Link to="/events">
          <EventsLink>Katso kaikki tapahtumat</EventsLink>
          <Arrow />
        </Link>
      )}
    </Container>
  );
};

export default UpcomingEvents;
