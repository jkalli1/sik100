import React from "react";
import styled from "../styled";

const Container = styled.div`
  background: rgba(0, 0, 0, 0.2);
  flex: 2;
  justify-self: flex-end;
  margin-left: 48px;
  border-radius: 12px;
  padding: 12px 24px;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    display: none;
  }
`;

const Text = styled.p`
  font-size: 18px;
  font-style: italic;
`;

const Author = styled.p`
  font-weight: bold;
  text-align: right;
`;

const Quote: React.FC = () => (
  <Container>
    <Text>
      ... että klubin jäsenillä aina tulisi olemaan hauskaa, että toverielämä kukoistaisi ja että myös sähkötekniikkaa klubissa harrastettaisiin.
    </Text>
    <Author>
      — Eitel Timgren
    </Author>
  </Container>
);

export default Quote;
