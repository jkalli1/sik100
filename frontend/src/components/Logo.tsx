import React from "react";
import logo from "../assets/SIK_W.svg";

const Logo: React.FC = () => (
  <img src={logo} alt="SIK" />
);

export default Logo;
