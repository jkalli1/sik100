import React from "react";
import styled from "../styled";
import GoldHR from "./GoldHR";
import Logo from "./Logo";

const Container = styled.div`
  color: ${(p) => p.theme.colors.white};
  display: flex;
  align-items: center;
  ${(p) => p.theme.margins.page};
  min-height: 200px;
  flex: 0 1;
`;

const LogoContainer = styled.div`
  max-width: 100px;
  flex: 1 0;
`;

const Footer: React.FC = () => (
  <>
    <GoldHR />
    <Container>
      <LogoContainer>
        <Logo />
      </LogoContainer>
      Copyright Aarni Halinen (aarni@sik100.fi)
    </Container>
  </>
);

export default Footer;
