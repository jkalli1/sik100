import React, { useState } from "react";
import styled from "../styled";
import GoldHR from "./GoldHR";

const Container = styled.div`
  ${(p) => (
    `
    background: linear-gradient(0deg, ${p.theme.colors.darkBlue2} 50%, ${p.theme.colors.blue5});
    display: flex;
    flex-direction: row nowrap;
    justify-content: center;
    align-items: center;
    padding: ${p.theme.spacing.md} ${p.theme.spacing.sm};
    text-decoration: uppercase;

    @media screen and (max-width: ${p.theme.breakpoints.xs}) {
      font-size: 12px;
      padding: ${p.theme.spacing.sm};
      justify-content: flex-end;
    }

    a {
      color: ${p.theme.colors.white};
      margin: 0 ${p.theme.spacing.xs};
      text-transform: uppercase;
      text-decoration: none;

      &:hover {
        /* color: ${p.theme.colors.yellow}; */
        text-decoration: underline;
      }

      @media screen and (max-width: ${p.theme.breakpoints.xs}) {
        display: none;
      }
    }
    `
  )}
`;

const MobileHamburger = styled.svg`
  min-height: 32px;
  min-width: 32px;
  fill: ${(p) => p.theme.colors.white};
  cursor: pointer;

  @media screen and (min-width: ${(p) => p.theme.breakpoints.xs}) {
    display: none;
  }
`;

const MobileLinkContainer = styled.div`
  display: flex;
  flex-direction: column;
  background: ${(p) => p.theme.colors.blue4};
  padding: ${(p) => p.theme.spacing.md} 0;

  a {
    color: ${(p) => p.theme.colors.white};
    margin: ${(p) => `${p.theme.spacing.xs} ${p.theme.spacing.md}`};
    border-bottom: ${(p) => `2px solid ${p.theme.colors.secondaryGold}`};
    text-transform: uppercase;
    text-decoration: none;

    &:hover {
      /* color: ${(p) => p.theme.colors.yellow}; */
    }
  }

  @media screen and (min-width: ${(p) => p.theme.breakpoints.xs}) {
    display: none;
  }
`;

const Sticky = styled.div`
  position: sticky;
  top: 0;
  z-index: 1000;
`;

type NavbarProps = {}

const Navbar: React.FC<NavbarProps> = ({ children }) => {
  const [isOpen, setOpen] = useState(false);
  return (
    <Sticky>
      {/* <> */}
      <Container>
        {children}
        <MobileHamburger
          onClick={(): void => { setOpen(!isOpen); }}
          role="img"
          viewBox="0 0 32 32"
          xmlns="http://www.w3.org/2000/svg"
        >
          <title>Menu</title>
          {/* eslint-disable-next-line */}
          <path d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z" />
        </MobileHamburger>
      </Container>
      <GoldHR />
      {isOpen && (
        <MobileLinkContainer>
          {children}
        </MobileLinkContainer>
      )}
      {/* </> */}
    </Sticky>
  );
};

export default Navbar;
