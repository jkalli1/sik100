import React, { useState } from "react";
import styled from "../styled";

type EventCardProps = {
  title: string;
  start: string;
  location: string;
  content: string[];
}

const EventContainer = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${(p) => p.theme.colors.blue3};
  margin: ${(p) => p.theme.spacing.sm} 0;
  box-shadow: 0 0 8px 0 ${(p) => p.theme.colors.primaryGold};
  cursor: pointer;
`;

const DateContainer = styled.div`
  flex: 1 0;
  background-color: ${(p) => p.theme.colors.primaryGold};
  color: ${(p) => p.theme.colors.darkBlue1};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${(p) => p.theme.fontSizes.biggest};
  font-weight: bold;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    font-size: ${(p) => p.theme.fontSizes.bigger};
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
    font-size: ${(p) => p.theme.fontSizes.h2};
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxxs}) {
    font-size: ${(p) => p.theme.fontSizes.normal};
  }
`;

const TextContainer = styled.div`
  flex: 4 0;
  ${(p) => `margin: ${p.theme.spacing.sm} ${p.theme.spacing.md};`}

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    flex: 3 0;
  }
`;

const Name = styled.p`
  color: ${(p) => p.theme.colors.turquoise};
  font-size: ${(p) => p.theme.fontSizes.bigger};
  margin: ${(p) => p.theme.spacing.xxs} 0;
  text-transform: uppercase;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    font-size: ${(p) => p.theme.fontSizes.h2};
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
    font-size: ${(p) => p.theme.fontSizes.normal};
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxxs}) {
    font-size: ${(p) => p.theme.fontSizes.small};
  }
`;

const Location = styled.p`
  color: ${(p) => p.theme.colors.white};
  font-size: ${(p) => p.theme.fontSizes.normal};
  margin: ${(p) => p.theme.spacing.xxs} 0;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxxs}) {
    font-size: ${(p) => p.theme.fontSizes.small};
  }
`;

const FundamentalContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
`;

const OpenableContent = styled.div`
  padding: ${(p) => `${p.theme.spacing.lg} ${p.theme.spacing.xlg}`};
  color: ${(p) => p.theme.colors.white};

  p:first-child {
    font-weight: bold;
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
    padding: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.md}`};
  }
`;

const ExpandableIcon = styled.div`
  font-size: 32px;
  height: 48px;
  width: 48px;
  text-align: center;
  justify-self: center;
  align-self: center;
  margin-right: 8px;
  color: ${(p) => p.theme.colors.white};
`;

const ContentP = styled.p`
  margin: 12px 0;
`;

export const EventCard: React.FC<EventCardProps> = ({
  title, start, location, content,
}) => {
  const [isOpen, setOpen] = useState(false);

  // console.log(content);
  const processedContent = content.map((p, idx) => (
    // eslint-disable-next-line react/no-array-index-key
    <ContentP key={idx}>
      {p}
    </ContentP>
  ));

  return (
    <EventContainer onClick={() => setOpen(!isOpen)}>
      <FundamentalContainer>
        <DateContainer>{start}</DateContainer>
        <TextContainer>
          <Name>{title}</Name>
          <Location>
            {`@${location}`}
          </Location>
        </TextContainer>
        <ExpandableIcon>
          {isOpen ? "-" : "+"}
        </ExpandableIcon>
      </FundamentalContainer>
      {isOpen && (
      <OpenableContent>
        {processedContent}
      </OpenableContent>
      )}
    </EventContainer>
  );
};

export default EventCard;
