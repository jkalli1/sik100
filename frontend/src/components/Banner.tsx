import React from "react";
import styled from "../styled";
import bg from "../assets/banner_bg.png";
import logo from "../assets/100_logo.svg";

const Container = styled.div`
  position: relative;
  background: url(${bg});
`;

const PageMargins = styled.div`
  ${(p) => p.theme.margins.page};

  @media screen and (max-width: ${(p) => p.theme.breakpoints.md}) {
    margin: 0;
  }
`;

const HeaderContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

const Logo = styled.img`
  flex: 1 0;
  max-width: 400px;
`;

const LogoContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: auto;
  padding: ${(p) => `${p.theme.spacing.lg} ${p.theme.spacing.xlg} ${p.theme.spacing.xlg}`};
  max-width: 1000px;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    padding: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.sm} ${p.theme.spacing.xlg}`};
    flex-direction: column;
  }
`;

const BannerAction = styled.div`
  flex: 1 0;
  margin-left: ${(p) => p.theme.spacing.lg};

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    margin-left: 0;
  }
`;

const Header = styled.h1`
  background-color: ${(p) => p.theme.colors.turquoise};
  position: absolute;
  bottom: -10%;
  text-align: center;
  text-transform: uppercase;
  font-weight: bold;
  padding: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.xlg}`};

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
    padding: ${(p) => `${p.theme.spacing.sm} 0`};
    width: 100%;
  }
`;

type BannerProps = {
  title?: string;
}

const Banner: React.FC<BannerProps> = ({ title, children }) => (
  <Container>
    <PageMargins>
      <LogoContainer>
        <Logo src={logo} />
        { children && (
          <BannerAction>
            {children}
          </BannerAction>
        )}
      </LogoContainer>
      { title && (
        <HeaderContainer>
          <Header>{title}</Header>
        </HeaderContainer>
      )}
    </PageMargins>
  </Container>
);

export default Banner;
