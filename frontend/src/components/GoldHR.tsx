import styled from "../styled";

const GoldHR = styled.div`
  background-color: ${(p) => p.theme.colors.secondaryGold};
  height: 8px;
`;

export default GoldHR;
