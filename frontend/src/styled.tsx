import styled, { CreateStyled } from "@emotion/styled";
import { ThemeType } from "./theme";

export default styled as CreateStyled<ThemeType>;
