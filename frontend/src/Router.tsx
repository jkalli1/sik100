import React from "react";
import {
  Switch,
  Route,
} from "react-router-dom";
import FrontPage from "./pages/FrontPage/FrontPage";
import ContactPage from "./pages/ContactPage/ContactPage";
import EventPage from "./pages/EventPage/EventPage";

export const Router: React.FC = () => (
  <Switch>
    <Route exact path="/">
      <FrontPage />
    </Route>
    <Route exact path="/events">
      <EventPage />
    </Route>
    <Route exact path="/sponsors">
      <ContactPage />
    </Route>
    <Route exact path="/blog">
      <ContactPage />
    </Route>
    <Route exact path="/contacts">
      <ContactPage />
    </Route>
  </Switch>
);

export default Router;
