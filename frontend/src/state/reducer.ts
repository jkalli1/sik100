import { ApplicationState } from "./state";
import { ActionType, ActionTypes } from "./actions";

export type ReducerType = (state: ApplicationState, action: ActionType) => ApplicationState;

const reducer: ReducerType = (state: ApplicationState, action: ActionType): ApplicationState => {
  switch (action.type) {
    case ActionTypes.COUNTER_INCREMENT:
      return {
        ...state,
        counter: state.counter + 1,
      };

    case ActionTypes.SET_EVENTS:
      return {
        ...state,
        events: action.payload.events,
      };
    default:
      return state;
  }
};

export default reducer;
