import React, {
  useContext, useReducer, FunctionComponent, ReactNode,
} from "react";
import { ReducerType } from "./reducer";
import { ActionType } from "./actions";
import Event from "./domain/events";

export interface ApplicationState {
  counter: number;
  events: Event[];
}

export const initialState: ApplicationState = {
  counter: 0,
  events: [],
};

type ReducerHook = [ApplicationState, React.Dispatch<ActionType>]

const StateContext = React.createContext({} as ReducerHook);

interface StateProviderProps {
  reducer: ReducerType;
  initState: ApplicationState;
  children: ReactNode;
}

export const StateProvider: FunctionComponent<StateProviderProps> = ({ reducer, initState, children }) => (
  <StateContext.Provider value={useReducer(reducer, initState)}>
    {children}
  </StateContext.Provider>
);

export const useStateValue = (): ReducerHook => useContext(StateContext);
