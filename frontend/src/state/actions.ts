export type ActionType = {
  type: ActionTypes;
  payload?: any;
};

export enum ActionTypes {
  COUNTER_INCREMENT = "COUNTER_INCREMENT",
  SET_EVENTS = "SET_EVENTS"
}

export default ActionTypes;
