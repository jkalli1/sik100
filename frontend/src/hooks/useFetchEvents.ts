import { useEffect } from "react";
import Axios from "axios";
import { useStateValue } from "../state/state";
import ActionTypes from "../state/actions";
import Event from "../state/domain/events";
import environment from "../environment";


const fetchData = async (): Promise<Event[]> => {
  if (environment.REACT_APP_BACKEND_HTTP_URL) {
    const url = `${environment.REACT_APP_BACKEND_HTTP_URL}/events`;
    const result = await Axios.get(url);
    return result.data;
  }
  throw new Error("No required runtime environment variables present!");
};

const useFetchEvents = (): void => {
  const [state, dispatch] = useStateValue();

  useEffect(() => {
    if (state.events.length === 0) {
      fetchData()
        .then((events) => {
          dispatch({ type: ActionTypes.SET_EVENTS, payload: { events } });
        })
        .catch((err) => {
          console.error(err);
        });
    }
  }, [state.events, dispatch]);
};

export default useFetchEvents;
