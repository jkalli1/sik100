import { useEffect, useState } from "react";
import Axios from "axios";
import environment from "../environment";

type InstagramObject = {
  img: string;
  link: string;
  text: string;
  id: string;
}

const fetchData = async (): Promise<InstagramObject[]> => {
  if (environment.REACT_APP_BACKEND_HTTP_URL && environment.REACT_APP_INSTAGRAM_IMAGE_COUNT) {
    const url = `${environment.REACT_APP_BACKEND_HTTP_URL}/instagram?count=${environment.REACT_APP_INSTAGRAM_IMAGE_COUNT}`;
    const result = await Axios.get(url);
    return result.data.data;
  }
  throw new Error("No required runtime environment variables present!");
};

const useFetchInstagram = (): InstagramObject[] => {
  const [images, setImages] = useState<InstagramObject[]>([]);
  useEffect(() => {
    fetchData()
      .then((data) => {
        setImages(data);
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);
  return images;
};

export default useFetchInstagram;
