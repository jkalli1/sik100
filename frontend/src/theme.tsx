
const fontSizes = {
  smaller: "10px",
  small: "12px",
  normal: "16px",
  h2: "24px",
  bigger: "36px",
  biggest: "48px",
};

const spacing = {
  xxs: "4px",
  xs: "8px",
  sm: "16px",
  md: "24px",
  lg: "48px",
  xlg: "96px",
};

const breakpoints = {
  xxxs: "375px",
  xxs: "480px",
  xs: "600px",
  sm: "840px",
  md: "1080px",
  lg: "1440px",
  xlg: "1920px",
};

export type ThemeType = {
  colors: {
    darkBlue1: string;
    darkBlue2: string;
    blue3: string;
    blue4: string;
    blue5: string;
    primaryGold: string;
    secondaryGold: string;
    yellow: string;
    sand: string;
    orange1: string;
    orange2: string;
    turquoise: string;
    white: string;
    black: string;
    // darkBlue: string;
    // lightBlue: string;
    // grey1: string;
    // grey2: string;
    // blue: string;
    // green: string;
  };
  margins: {
    page: string;
  };
  spacing: typeof spacing;
  breakpoints: typeof breakpoints;
  contentWidth: "1000px";
  fontSizes: typeof fontSizes;
}

export const theme: ThemeType = {
  colors: {
    darkBlue1: "#130D3F",
    darkBlue2: "#12123D",
    blue3: "#282561",
    blue4: "#19235D",
    blue5: "#24366D",
    primaryGold: "#FFD581",
    secondaryGold: "#FDF4A6",
    yellow: "#FEE469",
    sand: "#FEF9D9",
    orange1: "#F3783A",
    orange2: "#F49b61",
    // turquoise: "#BEDDEB",
    white: "#FFF",
    black: "#000",
    // Guild colors
    // darkBlue: "#002D3A",
    // lightBlue: "#BFDBD9",
    // grey1: "#D4D0C7",
    // grey2: "#EFECE4",
    // orange1: "#D57A2D",
    // orange2: "#DD934E",
    // blue: "#57B2DF",
    turquoise: "#BEDDEB",
    // green: "#C0DCD9",
    // sand: "#FDF9D7",
  },
  margins: {
    page: `
      margin: 0 ${spacing.xlg};
      @media screen and (max-width: ${breakpoints.xs}) {
        margin: 0 ${spacing.lg};
      }

      @media screen and (max-width: ${breakpoints.xxxs}) {
        margin: 0 ${spacing.md};
      }
      `,
  },
  spacing,
  breakpoints,
  contentWidth: "1000px",
  fontSizes,
};

export default theme;
