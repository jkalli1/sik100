import React from "react";
import { BrowserRouter, Link } from "react-router-dom";
import { ThemeProvider } from "emotion-theming";
import styled from "./styled";
import { theme } from "./theme";
import { StateProvider, initialState } from "./state/state";
import reducer from "./state/reducer";
import { Router } from "./Router";
import Countdown from "./components/Countdown";
import "./App.css";
import Navbar from "./components/Navbar";
import environment from "./environment";

const AppContainer = styled.div`
  background-color: ${(p) => p.theme.colors.darkBlue2};
  flex: 1 0;
`;

const App: React.FC = () => (
  <ThemeProvider theme={theme}>
    <StateProvider initState={initialState} reducer={reducer}>
      <AppContainer>
        <Countdown revealDate={environment.REACT_APP_REVEAL_DATE ? new Date(environment.REACT_APP_REVEAL_DATE) : new Date()}>
          <BrowserRouter>
            <Navbar>
              <Link to="/">Etusivu</Link>
              <Link to="/events">Tapahtumat</Link>
              <Link to="/sponsors">Yritysyhteistyö</Link>
              <Link to="/blog">Blogi</Link>
              <Link to="/contacts">Yhteystiedot</Link>
            </Navbar>
            <Router />
          </BrowserRouter>
        </Countdown>
      </AppContainer>
    </StateProvider>
  </ThemeProvider>
);

export default App;
