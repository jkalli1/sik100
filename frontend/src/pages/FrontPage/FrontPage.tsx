import React from "react";
import styled from "../../styled";
import { useStateValue } from "../../state/state";

import Banner from "../../components/Banner";
import Footer from "../../components/Footer";
import InstagramFeed from "../../components/InstgaramFeed";
import useFetchEvents from "../../hooks/useFetchEvents";
import PageContent from "../../components/PageContent";
import GoldHR from "../../components/GoldHR";
// import Sponsors from "../../components/Sponsors";
import UpcomingEvents from "../../components/UpcomingEvents";
import Quote from "../../components/Quote";

const EVENT_LIMIT = 3;

const HeroP = styled.p`
  font-size: 18px;
  color: white;
`;

const EventTitle = styled.h2`
  text-transform: uppercase;
  color: ${(p) => p.theme.colors.white};
`;

const UpcomingTitle = styled.h2`
  text-transform: uppercase;
  color: ${(p) => p.theme.colors.white};
  font-size: ${(p) => p.theme.fontSizes.h2};
  text-align: center;
`;

const EventContent = styled.div`
  display: flex;
  color: ${(p) => p.theme.colors.white};
`;

const P = styled.p`
  flex: 3;
`;

export const FrontPage: React.FC = () => {
  const [state] = useStateValue();
  useFetchEvents();
  return (
    <>
      <Banner>
        <HeroP>
          {/* eslint-disable-next-line max-len */}
          Aalto-yliopiston Sähköinsinöörikilta juhlistaa jo noin 100 vuotta kukoistanutta yhteistyötä opiskelijoiden, alumnien, yliopiston ja alan yritysten kesken.
          <br />
          <br />
          {/* eslint-disable-next-line */}
          Juhlavuoden aikana tullaan pitämään hauskaa, muistelemaan toverielämää kuin myös harrastamaan sähkötekniikkaa, että <strong>Eitel Timgrenin</strong> toive toteutuisi vielä ainakin seuraavat 100 vuotta.
        </HeroP>
      </Banner>
      <PageContent>
        <UpcomingEvents events={state.events.slice(0, EVENT_LIMIT)} link>
          <EventTitle>Tapahtumat</EventTitle>
          <EventContent>
            {/* eslint-disable-next-line max-len */}
            <P>SIK100 juhlistaa Aalto-yliopiston Sähköinsinöörikillan 100-vuotista taivalta vuoden mittaisella juhlavuodella, joka huipentuu 2021 helmikuussa Potentiaalin Tasaus 100:n. Kaikki tapahtumat löydät Tapahtuma-sivulta ja tässä alla on listattu 3 seuraavaa juhlavuoden tapahtumaa.</P>
            <Quote />
          </EventContent>
          <UpcomingTitle>Tulevat tapahtumat</UpcomingTitle>
        </UpcomingEvents>
      </PageContent>
      <GoldHR />
      {/* <Sponsors /> */}
      <PageContent>
        <InstagramFeed />
      </PageContent>
      <Footer />
    </>
  );
};

export default FrontPage;
