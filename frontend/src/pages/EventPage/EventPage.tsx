import React from "react";
import Banner from "../../components/Banner";
import PageContent from "../../components/PageContent";
import useFetchEvents from "../../hooks/useFetchEvents";
import { useStateValue } from "../../state/state";
import styled from "../../styled";
// import Sponsors from "../../components/Sponsors";
// import GoldHR from "../../components/GoldHR";
import Footer from "../../components/Footer";
import UpcomingEvents from "../../components/UpcomingEvents";

const BottomMargin = styled.div`
  margin-bottom: 200px;
`;

const EventPage: React.FC = () => {
  const [state] = useStateValue();
  useFetchEvents();
  return (
    <>
      <Banner title="Tapahtumat" />

      <BottomMargin>
        <PageContent>
          Mahdollinen saateteksti kalenterin päälle
          <UpcomingEvents events={state.events} />
        </PageContent>
      </BottomMargin>
      {/* <Sponsors /> */}
      {/* <GoldHR /> */}
      <Footer />
    </>
  );
};

export default EventPage;
