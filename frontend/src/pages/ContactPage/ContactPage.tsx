import React from "react";
import Banner from "../../components/Banner";
import styled from "../../styled";
import PageContent from "../../components/PageContent";
import contactsJson from "./contacts.json";

import Juuli from "../../assets/juuli.jpg";
import Elias from "../../assets/elias.jpg";
import Jami from "../../assets/jami.jpg";
import Olli from "../../assets/olli.jpg";
import Sasu from "../../assets/sasu.jpg";
import Tuukka from "../../assets/tuukka.jpg";
import Aarni from "../../assets/aarni.jpg";
import Lauri from "../../assets/lauri.jpg";
import Mikael from "../../assets/mikael.jpg";
import Miksu from "../../assets/miksu.jpg";
import Simo from "../../assets/simo.jpg";

const getImageFromEmail = (email: string): string => {
  switch (email.split("@")[0].toLowerCase()) {
    case "juuli":
      return Juuli;
    case "elias":
      return Elias;
    case "jami":
      return Jami;
    case "olli":
      return Olli;
    case "sasu":
      return Sasu;
    case "tuukka":
      return Tuukka;
    case "aarni":
      return Aarni;
    case "lauri":
      return Lauri;
    case "mikael":
      return Mikael;
    case "miksu":
      return Miksu;
    case "simo":
      return Simo;
    default:
      return "";
  }
};

const contacts = contactsJson.map((c) => (
  {
    ...c,
    image: getImageFromEmail(c.email),
  }
));

const HeroP = styled.p`
  font-size: 16px;
  color: white;
`;

const ContactsContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-evenly;
  margin-top: ${(p) => p.theme.spacing.xlg};
`;

const Contact = styled.div`
  margin: ${(p) => p.theme.spacing.lg};
  display: flex;
  flex-direction: column;
  align-items: center;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxxs}) {
    margin: ${(p) => p.theme.spacing.sm};
  }
`;

const Image = styled.img`
  width: 200px;
  height: 200px;
  border-radius: 100px;
  object-fit: cover;
  object-position: 0 -20px;
`;

const Info = styled.div`
  color: ${(p) => p.theme.colors.white};
  text-align: left;
  padding: ${(p) => p.theme.spacing.sm} 0;
  display: inline-block;
  max-width: 200px;
`;

const EmailLink = styled.a`
  color: ${(p) => p.theme.colors.white};
`;

export const ContactPage: React.FC = () => (
  <>
    <Banner title="Yhteystiedot">
      <HeroP>
        SIK100-juhlavuoden onnistumisesta on päävastuussa SIK:n toimihenkilöistä koostuva SIK100-toimikunta.
      </HeroP>
      <HeroP>
        30 henkistä toimikuntaa johtaa tältä sivulta löytyvät päävastuuhenkilöt.
      </HeroP>
      <HeroP>
        Toimikunnan tavoitat helpoiten lähettämällä sähköpostia osoitteeseen&nbsp;
        <EmailLink href="mailto:contact@sik100.fi">
          contact@sik100.fi
        </EmailLink>
.
      </HeroP>
    </Banner>
    <PageContent>
      <ContactsContainer>
        {contacts.map((contact) => (
          <Contact key={contact.name}>
            <Image src={contact.image} />
            <Info>
              <span>{contact.name}</span>
              <br />
              <span>{contact.role}</span>
              <br />
              <span>{contact.email}</span>
            </Info>
          </Contact>
        ))}
      </ContactsContainer>
    </PageContent>
  </>
);

export default ContactPage;
