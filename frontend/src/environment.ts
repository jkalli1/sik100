import dotenv from "dotenv";

dotenv.config();

export const environment = {
  ...process.env,
  ...((window as any).buildEnv || {}),
};

export default environment;
